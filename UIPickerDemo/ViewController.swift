//
//  ViewController.swift
//  UIPickerDemo
//
//  Created by Edy Cu Tjong on 6/24/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var picker: UIPickerView!
    
    var pickerData = [
        ["1", "2", "3", "4", "5", "6", "7"],
        ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
        ["!", "#", "$", "#"],
        ["w", "x", "y", "z"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.picker.delegate = self
        self.picker.dataSource = self
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("Component: \(component), Row: \(row)")
    }
}

